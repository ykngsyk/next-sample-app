# active-work

## セットアップ

### Docker

``` bash
# dockerを起動する
docker-compose up -d
```

## セットアップ

``` bash
$ docker exec -it next.sample.backend

# 1. マイグレーション実行
$ python manage.py migrate app

# 2. サンプルデータ取り込み
$ python manage.py loaddata init
```

## 静的解析/フォーマッター

### バックエンド

``` bash
$ docker exec -it next.sample.backend bash
# 静的解析
$ find . -name "*.py" | xargs -I{} flake8 {} --ignore=E501,W503
# フォーマッター
$ black .
```

### フロントエンド

``` bash
$ docker exec -it next.sample.frontend bash
# [ コード ]
# 静的解析のみ
$ npm run lint
# 静的解析 + フォーマッター
$ npm run lintfix

# [ スタイルシート ]
# 静的解析のみ
$ npm run lint:style
# 静的解析 + フォーマッター
$ npm run lintfix:style
```
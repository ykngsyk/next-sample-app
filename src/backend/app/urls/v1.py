from django.urls import path
from app.views import v1 as views

urlpatterns = [path(r"/articles", views.ArticleView.as_view())]

from django.db import transaction
from django.http import HttpResponse
from django.http import QueryDict

from app.lib.views.view import ApiView
from app.models import Article


class ArticleView(ApiView):
    def get(self, request):
        articles = Article.objects.all()
        self.set("articles", [article.to_dict() for article in articles])
        return self.response()

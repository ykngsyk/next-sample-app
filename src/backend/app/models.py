from django.db import models
from django.contrib.postgres.fields import JSONField
import stringcase

class BaseModel(models.Model):
    def to_dict(self, deep=False):
        return {
            stringcase.camelcase(field.name): getattr(self, field.name)
            for field in self._meta.fields
            if not isinstance(field, models.ForeignKey)
        }

    class Meta:
        abstract = True


class Article(BaseModel):
    title = models.TextField()
    text = models.TextField()

    class Meta:
        db_table = "app_article"
        ordering = ["id"]
import React from 'react'
import { useRouter } from 'next/router'

const TopPage: React.FC = () => {
  const router = useRouter()
  const handleClick = () => {
    router.push('/articles')
  }

  return (
    <div>
      <h1>TOP</h1>
      <button onClick={handleClick}>移動する</button>
    </div>
  )
}

export default TopPage

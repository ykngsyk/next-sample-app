import styled from 'styled-components'

const Footer = styled.footer`
  width: 100%;
  height: 40px;
  background-color: #ddd;
`

const PageFooter = () => <Footer></Footer>

export default PageFooter

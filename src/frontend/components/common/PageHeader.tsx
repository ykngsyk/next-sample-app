import styled from 'styled-components'

const Header = styled.header`
  width: 100%;
  height: 40px;
  background-color: #ddd;
`

const PageHeader = () => <Header></Header>

export default PageHeader

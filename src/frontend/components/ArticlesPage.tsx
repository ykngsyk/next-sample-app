import React from 'react'
import { connect } from 'react-redux'
import { articleOperations, articleSelectors } from '@/store/modules/Article'

type Article = {
  id: number
  title: string
  text: string
}

const mapStateToProps = state => {
  return {
    articles: articleSelectors.getArticles(state)
  }
}

const mapDispatchToProps = {
  fetch: articleOperations.fetch
}

type Props = {
  fetch: () => void
  articles: Article[]
}

const ArticleList: React.FC<Props> = ({ fetch, articles }: Props) => (
  <div>
    <h1>ARTICLES</h1>
    <button onClick={() => fetch()}>取得する</button>
    {articles.map(article => (
      <div key={`article-${article.id}`}>
        <div>{article.title}</div>
        <div>{article.text}</div>
      </div>
    ))}
  </div>
)

export default connect(mapStateToProps, mapDispatchToProps)(ArticleList)

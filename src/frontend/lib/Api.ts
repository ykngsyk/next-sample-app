import axios, { AxiosResponse } from 'axios'

interface ApiResponseJson<T> {
  status: string
  result: T
}

export class ApiResponse<T> {
  status: string
  result: T

  public constructor({ status, result }: ApiResponseJson<T>) {
    this.status = status
    this.result = result
  }
}

class Api {
  public async get<T>(
    url: string,
    option: Record<string, any>
  ): Promise<ApiResponse<T>> {
    let response: AxiosResponse
    try {
      response = await axios.get(url, option)
    } catch (error) {}

    let apiResponse: ApiResponse<T>
    try {
      apiResponse = new ApiResponse(response.data)
    } catch (error) {
      return apiResponse
    }

    return apiResponse
  }
}

export const api = new Api()

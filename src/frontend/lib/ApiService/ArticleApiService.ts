import { ApiService } from './ApiService'

export class ArticleApiService extends ApiService {
  async getArticles() {
    const response = await this.get<{ articles: [] }>('articles')
    return response.result.articles
  }
}

import { api, ApiResponse } from '@/lib/Api'

export class ApiService {
  host: string
  baseUrl: string

  public constructor() {
    this.host = 'http://localhost:8003'
    this.baseUrl = '/api/v1'
  }

  public buildUrl(path: string): string {
    return `${this.host}${this.baseUrl}/${path}`
  }

  public async get<T>(path: string, params = {}): Promise<ApiResponse<T>> {
    return await api.get<T>(this.buildUrl(path), params)
  }
}

export const apiService = new ApiService()

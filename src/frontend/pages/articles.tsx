import Layout from '@/layouts/main'
import ArticlesPage from '@/components/ArticlesPage'

export default () => (
  <Layout>
    <ArticlesPage />
  </Layout>
)

import Layout from '@/layouts/main'
import TopPage from '@/components/TopPage'

export default () => (
  <Layout>
    <TopPage />
  </Layout>
)

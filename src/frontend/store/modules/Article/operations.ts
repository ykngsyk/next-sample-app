import actions from './actions'
import { ArticleApiService } from '@/lib/ApiService/ArticleApiService'

const apiService = new ArticleApiService()

const fetch = () => {
  return async dispatch => {
    const articles = await apiService.getArticles()
    dispatch(actions.setArticles(articles))
  }
}

export default {
  fetch
}

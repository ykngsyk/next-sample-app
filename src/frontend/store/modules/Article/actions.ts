import types from './types'

const setArticles = articles => {
  return {
    type: types.SET_ARTICLES,
    articles
  }
}

export default {
  setArticles
}

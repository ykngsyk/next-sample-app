import reducer from './reducers'

import { default as articleTypes } from './types'
import { default as articleActions } from './actions'
import { default as articleOperations } from './operations'
import { default as articleSelectors } from './selectors'

export { articleTypes, articleActions, articleOperations, articleSelectors }

export default reducer

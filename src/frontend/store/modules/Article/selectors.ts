const getArticles = state => {
  return state.Article.article.articles
}

export default {
  getArticles
}

import { combineReducers } from 'redux'
import types from './types'

const initialState = {
  articles: []
}

const articleReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_ARTICLES:
      return Object.assign({}, state, { articles: action.articles })
    default:
      return state
  }
}

export default combineReducers({
  article: articleReducer
})

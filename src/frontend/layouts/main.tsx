import { ReactNode } from 'react'
import styled from 'styled-components'
import PageHeader from '@/components/common/PageHeader'
import PageFooter from '@/components/common/PageFooter'

const PageLayout = styled.div``

const PageWrapper = styled.div`
  padding: 10px;
`

interface Props {
  children?: ReactNode
}

export default ({ children }: Props) => (
  <PageLayout>
    <PageHeader />
    <PageWrapper>{children}</PageWrapper>
    <PageFooter />
  </PageLayout>
)
